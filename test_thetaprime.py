import numpy as np
import thetaprime
import matplotlib.pyplot as plt

thetas = np.linspace(-np.pi, np.pi)
# print thetas
thetasmalls = np.linspace(-np.pi/100, np.pi/100)
# print thetasmalls

thetagenerals = thetaprime.general(thetas, 20, 10, 0.1)
# print thetaprimes
thetafinals = thetaprime.final(thetasmalls, 10, 0.1)
# print thetasmallprimes

plt.subplot('211')
plt.plot(thetasmalls, thetagenerals)
plt.subplot('212')
plt.plot(thetasmalls, thetafinals)
plt.show()