import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import gridspec
from PIL import Image
from matplotlib.widgets import Slider, Button, RadioButtons
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from decimal import Decimal

import thetaprime

img_choice = {'Hubble Ultra Deep Field':'ultra_space.jpg','Andromeda Galaxy':'andromeda.jpg','Square Grid':'grid.jpg','Mona Lisa':'mona.jpg'}
marker_choice = ['None','X','Galaxy','The Moon'] 

class params:
	def __init__(self):
		self.D_OL = 0.5*10**10 #distance from observer to lens
		self.D_LS = 0.5*10**10 #distance from lens to sky
		self.SS = 4.0*10**6 #size of the sky (width of image)
		
		self.M = 1.0*10**12 #mass of object
		#self.c_M = (np.round(img_in.shape[1]/2),np.round(img_in.shape[0]/2)) #pixel coordinates of center of mass projected on screen
		self.c_M=(0,0)
		
		self.img_name_cur='ultra_space.jpg'
		#self.img_name_cur='grid.jpg'
		#self.img_name_cur='mona.jpg'
		self.img_in = jpg_image_to_array(self.img_name_cur)
		self.img_name_prev=self.img_name_cur
		
		self.marker_type=marker_choice[1]
		
		self.dx_S = self.SS/(self.img_in.shape[1]) #length of 'sky' for each pixel of input image

def imscatter(x, y, image, ax=None, zoom=1):
    if ax is None:
        ax = plt.gca()
    try:
        image = plt.imread(image)
    except TypeError:
        # Likely already an array...
        pass
    im = OffsetImage(image, zoom=zoom)
    x, y = np.atleast_1d(x, y)
    artists = []
    for x0, y0 in zip(x, y):
        ab = AnnotationBbox(im, (x0, y0), xycoords='data', frameon=False)
        artists.append(ax.add_artist(ab))
    ax.update_datalim(np.column_stack([x, y]))
    ax.autoscale()
    return artists		

def jpg_image_to_array(image_path):
	"""
	Loads JPEG image into 3D Numpy array of shape 
	(width, height, channels)
	"""
	with Image.open(image_path) as image:         
		im_arr = np.fromstring(image.tobytes(), dtype=np.uint8)
		if im_arr.size==image.size[1]*image.size[0]:
			im_arr = im_arr.reshape((image.size[1], image.size[0], 1))
			return np.repeat(im_arr,3,axis=2)
		im_arr = im_arr.reshape((image.size[1], image.size[0], 3))
		return im_arr

		
def set_image(p):
	if p.img_name_cur!=p.img_name_prev:
		p.img_in = jpg_image_to_array(p.img_name_cur)
		p.img_name_prev=p.img_name_cur

def set_img_marker(p):
	if hasattr(p,'sct'):
		if isinstance(p.sct, list):
			p.sct[0].remove()
		else:
			p.sct.remove()
	if p.marker_type==marker_choice[0]:
		p.sct=p.ax.scatter([p.c_M[0]], [p.c_M[1]], c='b', s=0, marker ='x')
	elif p.marker_type==marker_choice[1]:
		p.sct=p.ax.scatter([p.c_M[0]], [p.c_M[1]], c='r', s=100, marker ='x')
	elif p.marker_type==marker_choice[2]:
		p.sct=imscatter([p.c_M[0]], [p.c_M[1]],'galaxy.png',ax=p.ax,zoom=.4)
	elif p.marker_type==marker_choice[3]:
		p.sct=imscatter([p.c_M[0]], [p.c_M[1]],'super-moon.png',ax=p.ax,zoom=0.1)
	
	p.ax.set_xlim([0,p.img_in.shape[1]-1])
	p.ax.set_ylim([p.img_in.shape[0]-1,0])

def gen_image(p):
	
	set_image(p)
	
	#note the order of x and y
	lx=np.linspace(0.,p.img_in.shape[1]-1.,p.img_in.shape[1])
	ly=np.linspace(0.,p.img_in.shape[0]-1.,p.img_in.shape[0])
	absx, absy = np.meshgrid(lx, ly)
	relx=absx-p.c_M[0]
	rely=absy-p.c_M[1]

	relr=np.sqrt(relx**2+rely**2)*p.dx_S #radius from center p.c_M

	#print str(np.amax(p.img_in))
	#np.place(p.img_in,np.repeat(relr,3,axis=1)<100,0)

	theta = np.arcsin(  relr/np.sqrt(relr**2+(p.D_OL+p.D_LS)**2)  )
	thetap = thetaprime.smallreal(theta,p.D_LS+p.D_OL,p.D_OL,p.M)

	newr = np.sqrt( ((p.D_LS+p.D_OL)**2 * np.sin(thetap)**2 ) / (1-np.sin(thetap)**2) )
	newr=np.nan_to_num(newr)

	ratio_r = np.sign(thetap)*newr/relr #sign accounts for angle sign flip
	ratio_r = np.nan_to_num(ratio_r)

	relx_new = relx*ratio_r
	rely_new = rely*ratio_r
	absx_new = np.round(relx_new+p.c_M[0])
	absy_new = np.round(rely_new+p.c_M[1])
	absx_new=absx_new.astype(int)
	absy_new=absy_new.astype(int)

	img_out=np.zeros(p.img_in.shape)
	temp=p.img_in[0,0,:]
	#p.img_in[0,0,:]=0 #adhoc default value

	OOB_mask = np.where( np.logical_or(absy_new>=p.img_in.shape[0],np.logical_or(absy_new<0,np.logical_or(absx_new>=p.img_in.shape[1],absx_new<0))) )
	absy_new[OOB_mask]=0
	absx_new[OOB_mask]=0

	img_out=p.img_in[absy_new,absx_new,:]

	img_out=img_out.astype(np.uint8)
	p.img_in[0,0,:]=temp
	return img_out
	

print '\n\nRunning...\n\n'

p=params()

fig = plt.figure(figsize=(10,10))

gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1]) 
#p.ax = fig.add_subplot(gs[0])
p.ax = plt.subplot(gs[0])
p.ax.axis('off')
fig.subplots_adjust(left=0.0, bottom=0.00)



#img_out=gen_image(p)
p.imgplot = p.ax.imshow(p.img_in)
set_img_marker(p)

axcolor = 'lightgoldenrodyellow'

ax_M = fig.add_axes([0.755, 0.75, 0.2, 0.03], facecolor=axcolor)
ax_D_OL = fig.add_axes([0.755, 0.70, 0.2, 0.03], facecolor=axcolor)
ax_D_T = fig.add_axes([0.755, 0.65, 0.2, 0.03], facecolor=axcolor)
ax_SS = fig.add_axes([0.755, 0.60, 0.2, 0.03], facecolor=axcolor)
ax_bt_gen  = fig.add_axes([0.755, 0.15, 0.2, 0.03], facecolor=axcolor)
ax_bt_res  = fig.add_axes([0.755, 0.1, 0.2, 0.03], facecolor=axcolor)
ax_rbt_img = fig.add_axes([0.755, 0.2, 0.2, 0.1], facecolor=axcolor)
ax_rbt_mrk = fig.add_axes([0.755, 0.32, 0.2, 0.1], facecolor=axcolor)

sl_M = Slider(ax_M, 'Solar Masses', -1, 20, valinit=np.log10(p.M))
sl_M.valtext.set_text('%.2E' % Decimal(p.M))
sl_D_OL = Slider(ax_D_OL, 'Distance to Mass', -3, 10, valinit=np.log10(p.D_OL))
sl_D_OL.valtext.set_text('%.2E' % Decimal(p.D_OL))
sl_D_T = Slider(ax_D_T, 'Distance to Source', -1, 10, valinit=np.log10(p.D_OL+p.D_LS))
sl_D_T.valtext.set_text('%.2E' % Decimal(p.D_OL+p.D_LS))
sl_SS = Slider(ax_SS, 'Source Width', -1, 10, valinit=np.log10(p.SS))
sl_SS.valtext.set_text('%.2E' % Decimal(p.SS))

bt_gen = Button(ax_bt_gen, 'Generate Image', color=axcolor, hovercolor='0.975')
bt_res = Button(ax_bt_res, 'Reset Image', color=axcolor, hovercolor='0.975')

#rbt_img = RadioButtons(ax_rbt_img, ('Hubble Ultra Deep Field', 'Andromeda Galaxy', 'Square Grid', 'Mona Lisa'), active=0)
rbt_img = RadioButtons(ax_rbt_img, img_choice.keys(),active=1)
rbt_mrk = RadioButtons(ax_rbt_mrk, marker_choice,active=1)

def fig_update(val):
	p.M=10**sl_M.val
	sl_M.valtext.set_text('%.2E' % Decimal(p.M))
	p.D_OL=10**sl_D_OL.val
	sl_D_OL.valtext.set_text('%.2E' % Decimal(p.D_OL))
	p.D_LS=10**sl_D_T.val-p.D_OL
	sl_D_T.valtext.set_text('%.2E' % Decimal(p.D_OL+p.D_LS))
	p.SS=10**sl_SS.val
	p.dx_S = p.SS/(p.img_in.shape[1])
	sl_SS.valtext.set_text('%.2E' % Decimal(p.SS))
sl_M.on_changed(fig_update)
sl_D_OL.on_changed(fig_update)
sl_D_T.on_changed(fig_update)
sl_SS.on_changed(fig_update)

def fig_regen(val):
	img_out=gen_image(p)
	p.imgplot.set_data(img_out)
	fig.canvas.draw()
bt_gen.on_clicked(fig_regen)

def fig_reset(val):
	p.imgplot.set_data(p.img_in)
	fig.canvas.draw()
bt_res.on_clicked(fig_reset)

def fig_pick_img(label):
	#img_choice = {'Hubble Ultra Deep Field':'ultra_space.jpg','Andromeda Galaxy':'andromeda.jpg','Square Grid':'grid.jpg','Mona Lisa':'mona.jpg'}
	p.img_name_cur=img_choice[label]
	set_image(p)
	#p.imgplot.set_data(p.img_in)
	p.imgplot.remove()
	p.imgplot = p.ax.imshow(p.img_in)
	p.ax.set_xlim([0,p.img_in.shape[1]-1])
	p.ax.set_ylim([p.img_in.shape[0]-1,0])
	p.dx_S = p.SS/(p.img_in.shape[1])
	fig.canvas.draw()
rbt_img.on_clicked(fig_pick_img)

def fig_pick_mrk(label):
	p.marker_type=label
	set_img_marker(p)
	fig.canvas.draw()
rbt_mrk.on_clicked(fig_pick_mrk)

def fig_onclick(event):
	if event.inaxes != p.ax:
		return
	# print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f, axis=%s' %
		# ('double' if event.dblclick else 'single', event.button,
		# event.x, event.y, event.xdata, event.ydata, event.inaxes))
	
	p.c_M=(event.xdata,event.ydata)
	set_img_marker(p)
	fig.canvas.draw()
cid = fig.canvas.mpl_connect('button_press_event', fig_onclick)

plt.tight_layout()
mng = plt.get_current_fig_manager()
mng.window.state('zoomed')
#plt.axis('off')
plt.show()



