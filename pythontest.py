import time
import numpy
import matplotlib

# Does this become a comment properly?

testTime = time.time()

# print("Hello, World!")
# inputTestNum = float(input("Put number here: "))
# print("The number you put was: %f" %inputTestNum)

# print("The time was: %f" %testTime)

def tic():
	# Stackoverflowed version of tic() command from MatLAB
	global startTimeForTictoc
	startTimeForTictoc = time.time()

def toc():
	# Requires import time if used elsewhere
	# Stackoverflowed version of toc() command from MatLAB (to use with tic())
	if "startTimeForTictoc" in globals():
		print "Elapsed time is " + str(time.time() - startTimeForTictoc) + " seconds."
	else:
		print "Toc: start time not set."
		
tic()

print "Test"
time.sleep(5)
print "Test again"

toc()