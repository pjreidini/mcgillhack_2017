import numpy as np

def small(thetaQuery, D_S, D_L, M):
	# Parameters:
	# thetaQuery: angle on the final image we want
	# D_S: Distance from the Observer to the Source (Image)
	# D_L: Distance from the Observer to the Lens
	# M: Mass of the Lens
	#
	# Output:
	# thetaPrime: The angle on the Source (Image) that produces thetaQuery at the Observer
	#
	# OBS:
	# If thetaQuery is zero (ray passes through mid of lens) you get infinities. Don't do that.

	G = 1.0
	c = 1.0
	
	# einsteinTheta = np.sqrt(4*G*M*D_LS/(np.pow(c, 2)*D_L*D_S))
	
	# Calculate 'path' of ray shot from eye
	
	# Calculate impact parameter
	xsi = thetaQuery * D_L				# Assume xsi is small, take tan approximation
	alpha = (4 * G * M) / (c * c * xsi)	# Calculate deflection
	thetaStar = thetaQuery - alpha		# Calculate deflected angle
	r = xsi + (D_S - D_L) * thetaStar			# Calculate distance of image point from center
	
	return (r / D_S)

	
def general(thetaQuery, D_S, D_L, M):
	# Parameters:
	# thetaQuery: angle on the final image we want
	# D_S: Distance from the Observer to the Source (Image)
	# D_L: Distance from the Observer to the Lens
	# M: Mass of the Lens
	#
	# Output:
	# thetaPrime: The angle on the Source (Image) that produces thetaQuery at the Observer
	#
	# OBS:
	# If thetaQuery is zero (ray passes through mid of lens) you get infinities. Don't do that.
	
	G = 1.0
	c = 1.0
	
	# Calculate 'path' of ray shot from eye
	
	# Calculate impact parameter
	xsi = np.tan(thetaQuery) * D_L			# Assume xsi is small, take tan approximation
	alpha = (4.0 * G * M) / (c * c * xsi)		# Calculate deflection
	thetaStar = thetaQuery - alpha			# Calculate deflected angle
	r = xsi + (D_S - D_L) * np.tan(thetaStar)	# Calculate distance of image point from center
	
	return np.arctan(r / D_S)
	
def final(thetaQuery, D_L, M):
    
    G = 1.0
    c = 1.0
        
    # Calculate 'path' of ray shot from eye
        
    # Calculate impact parameter
    xsi = np.tan(thetaQuery) * D_L			# Assume xsi is small, take tan approximation
    alpha = ((4.0 * G * M) / (c * c * xsi))		# Calculate deflection
    return thetaQuery - alpha
	
# def real(thetaQuery, D_L, M):
	# # distances in light years, mass in solar masses

	# G = 0.0000000000667300				# m3 kg-1 s-2
	# c = 299792458						# m s-1
	# LYtom = 9460730472580800			# one light year in meters
	# SMtokg = 1.98855 * np.pow(10, 30)	# one solar mass in kg
	
	# xsi = np.tan(thetaQuery) * D_L * LYtom
	# alpha = ((4 * G * M * SMtokg) / (c * c * xsi))
	# return thetaQuery - alpha
	
	
def real(thetaQuery, D_S, D_L, M):
	# distances in light years, mass in solar masses

	G = 0.0000000000667300			# m3 kg-1 s-2
	c = 299792458.0						# m s-1
	LYtom = 9460730472580800.0			# one light year in meters
	SMtokg = 1.98855 * (10 ** 30)		# one solar mass in kg
	
	xsi = np.tan(thetaQuery) * D_L * LYtom
	alpha = ((4.0 * G * M * SMtokg) / (c * c * xsi))
	thetaStar = thetaQuery - alpha			# Calculate deflected angle
	r = xsi + (D_S - D_L) * LYtom * np.tan(thetaStar)	# Calculate distance of image point from center
	
	return np.arctan(r / (D_S * LYtom))

	
def smallreal(thetaQuery, D_S, D_L, M):
	# Parameters:
	# thetaQuery: angle on the final image we want
	# D_S: Distance from the Observer to the Source (Image)
	# D_L: Distance from the Observer to the Lens
	# M: Mass of the Lens
	#
	# Output:
	# thetaPrime: The angle on the Source (Image) that produces thetaQuery at the Observer
	#
	# OBS:
	# If thetaQuery is zero (ray passes through mid of lens) you get infinities. Don't do that.

	G = 0.0000000000667300			# m3 kg-1 s-2
	c = 299792458.0						# m s-1
	LYtom = 9460730472580800.0			# one light year in meters
	SMtokg = 1.98855 * (10 ** 30)		# one solar mass in kg
	
	# einsteinTheta = np.sqrt(4*G*M*D_LS/(np.pow(c, 2)*D_L*D_S))
	
	# Calculate 'path' of ray shot from eye
	
	# Calculate impact parameter
	xsi = thetaQuery * D_L * LYtom				# Assume xsi is small, take tan approximation
	alpha = (4.0 * G * M * SMtokg) / (c * c * xsi)	# Calculate deflection
	thetaStar = thetaQuery - alpha		# Calculate deflected angle
	r = xsi + (D_S - D_L) * LYtom * thetaStar			# Calculate distance of image point from center
	
	return (r / (D_S * LYtom))